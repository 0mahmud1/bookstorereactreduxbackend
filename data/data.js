var mongoose = require("mongoose");
var Schema = mongoose.Schema({
    name: String,
    email: String,
    contact : String
});

module.exports = mongoose.model("data", Schema);